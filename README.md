# Installing Rust

> IMPORTANT: This will download and install Rust without asking for confirmation

`curl https://sh.rustup.rs -sSf | sh -s -- -y`

# Compiling for maximum performance

`cargo b --profile release_with_lto`

# Compiling for debug

`cargo b`

# Installing globally

`cargo install --path . --profile release_with_lto`

# Running from cargo

Just change any `cargo b` to `cargo r` and instead of only building it will also immediatly run after build
