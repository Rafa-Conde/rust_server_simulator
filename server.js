const http = require('http');
const fs = require('fs');
const port = 3000;

function streamFile(file, request, response, header) {

  if (!fs.existsSync(file.path)) {
    console.log(`File ${file.name} Not Found`);
    response.writeHead(400);
    response.write('File Not Found');
    response.end();
    return;
  }
  if (file.extension === 'bin') {
    // console.log(request.headers)
    var info = fs.statSync(file.path);
    header = {
      'Content-disposition': "attachment; filename=" + file.path,
      'Content-Length': info.size
    }
    console.log(header)
  }
  console.log(`Sending ${file.type} file -> ${file.name}`)
  var stream = fs.createReadStream(file.path);
  response.writeHead(200, header)
  stream.pipe(response)
}

function handlePost(request, response) {
  console.log(request.method)
  if (request.method === 'POST') {
    console.log(request.headers);
    var body = "";
    request.on('data', chunk => {
      body += chunk;
    });
    request.on('end', () => {
      console.log('POSTed: ' + body);
      response.writeHead(200);
      response.end("updated");
    })
    return;
  }

  response.writeHead(200);
  response.end();

}

const requestListener = (req, res) => {
  /* Ignore Icon */
  if (req.url === '/favicon.ico') return;

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Request-Method', '*');
  res.setHeader('Access-Control-Allow-Methods', 'POST, OPTIONS, GET');
  res.setHeader('Access-Control-Allow-Headers', '*');

  if (req.url === '/UberSmart/stats') {
    handlePost(req, res);
    return;
  }
  /* Variables */
  file = { 'name': req.url };
  file.path = file.name.replace(/^\/+/, '');
  regex = new RegExp('[^.]+$');
  file.extension = file.name.match(regex).toString();

  /* Select response based on extension */
  switch (file.extension) {

    case 'js':
      file.type = 'JS';
      header = { 'Content-Type': 'text/javascript' };
      streamFile(file, req, res, header);
      break;

    case 'css':
      file.type = 'CSS';
      header = { 'Content-Type': 'text/css' };
      streamFile(file, req, res, header);
      break;

    case 'png':
      file.type = 'Image';
      header = { 'Content-Type': 'image/png' };
      streamFile(file, req, res, header);
      break;

    case 'json':
      file.type = 'JSON'
      header = { 'Content-Type': 'application/json' }
      streamFile(file, req, res, header);
      break;

    case 'bin':
      file.type = 'BIN';
      header = {}
      streamFile(file, req, res, header);
      break;

    default:
      file.type = 'Index';
      header = { 'Content-Type': 'text/html' };
      streamFile(file, req, res, header);
      break;
  }

}

const server = http.createServer(requestListener);

server.listen(port, function (error) {
  if (error) {
    console.log('Something Went Wrong');
  } else {
    console.log('Version v0.4')
    console.log('Server is listen on port ' + port);
  }
})