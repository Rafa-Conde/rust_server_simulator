#![warn(clippy::all,
clippy::pedantic,
clippy::perf,
clippy::nursery,
// clippy::cargo,
clippy::unwrap_used,
clippy::expect_used)]

use axum::{
    body::{Body, Bytes},
    extract::{MatchedPath, Path},
    http::{header, HeaderMap, Request, StatusCode},
    response::{IntoResponse, Response},
    routing::{get, post},
    Router,
};
use clap::{command, Parser};
use std::{path::PathBuf, str::FromStr, time::Duration};
use tokio_util::io::ReaderStream;
use tower_http::{classify::ServerErrorsFailureClass, trace::TraceLayer};
use tracing::{info, info_span, Span};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

// `&'static str` becomes a `200 OK` with `content-type: text/plain; charset=utf-8`
async fn uber_smart(body: String) {
    println!("POSTed: {body}");
}

async fn get_root(root_path: PathBuf) -> Result<impl IntoResponse, impl IntoResponse> {
    get_file(Path(String::from("index.html")), root_path).await
}

// `Json` gives a content-type of `application/json` and works with any type
// that implements `serde::Serialize`
async fn get_file(
    Path(file): Path<String>,
    root_path: PathBuf,
) -> Result<impl IntoResponse, impl IntoResponse> {
    println!("Getting file {file}");
    let file_name = file.clone();
    let mut path = root_path.join(file);
    if !path.exists() {
        if path.set_extension("html") {
            if !path.exists() {
                println!("Path didn't existed: {:?}", path.to_str());
                return Err((StatusCode::NOT_FOUND, "File Not Found"));
            }
        } else {
            println!("Path didn't existed: {:?}", path.to_str());
            return Err((StatusCode::NOT_FOUND, "File Not Found"));
        }
    }

    let extension = path.extension().map_or_else(
        || String::from("bin"),
        |extension| extension.to_string_lossy().to_string(),
    );

    let file = match tokio::fs::File::open(path).await {
        Ok(file) => file,
        Err(err) => {
            println!("Error opening the file: {err}");
            return Err((
                StatusCode::INTERNAL_SERVER_ERROR,
                "File couldn't be read by the server",
            ));
        }
    };

    let file_size = match file.metadata().await {
        Ok(metadata) => metadata.len(),
        Err(err) => {
            println!("Error getting file size: {err}");
            return Err((
                StatusCode::INTERNAL_SERVER_ERROR,
                "Error getting the file size",
            ));
        }
    };

    let stream = ReaderStream::new(file);

    let body = Body::from_stream(stream);

    let mut headers = HeaderMap::new();

    #[allow(clippy::unwrap_used)]
    match extension.as_str() {
        "js" => headers.insert(header::CONTENT_TYPE, "text/javascript".parse().unwrap()),
        "css" => headers.insert(header::CONTENT_TYPE, "text/css".parse().unwrap()),
        "png" => headers.insert(header::CONTENT_TYPE, "image/png".parse().unwrap()),
        "jpg" => headers.insert(header::CONTENT_TYPE, "image/jpg".parse().unwrap()),
        "json" => headers.insert(header::CONTENT_TYPE, "application/json".parse().unwrap()),
        "bin" => {
            headers.insert(
                header::CONTENT_DISPOSITION,
                format!("attachment; filename={file_name}").parse().unwrap(),
            );
            headers.insert(
                header::CONTENT_LENGTH,
                file_size.to_string().parse().unwrap(),
            )
        }
        _ => headers.insert(header::CONTENT_TYPE, "text/html".parse().unwrap()),
    };

    Ok((headers, body))
}

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Port to open the server
    #[arg(short, long, default_value_t = 3000)]
    port: u16,
    /// Root path for finding the files
    #[arg(short, long, default_value_t = std::env::current_dir().unwrap().to_string_lossy().to_string())]
    root_path: String,
}

#[tokio::main]
async fn main() {
    let args = Args::parse();
    let port = args.port;
    println!("Current root dir is: {}", args.root_path);
    let root_get = PathBuf::from_str(&args.root_path).unwrap();
    let root_get_root = PathBuf::from_str(&args.root_path).unwrap();

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| {
                // axum logs rejections from built-in extractors with the `axum::rejection`
                // target, at `TRACE` level. `axum::rejection=trace` enables showing those events
                "server_simulator=trace,tower_http=trace,axum::rejection=trace".into()
            }),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let get_file_closure = move |path| get_file(path, root_get);
    let get_root_closure = move || get_root(root_get_root);

    let app = Router::new()
        .route("/*file", get(get_file_closure))
        .route("/", get(get_root_closure))
        .route("/UberSmart/stats", post(uber_smart))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(|request: &Request<_>| {
                    // Log the matched route's path (with placeholders not filled in).
                    // Use request.uri() or OriginalUri if you want the real path.
                    let matched_path = request
                        .extensions()
                        .get::<MatchedPath>()
                        .map(MatchedPath::as_str);

                    info_span!(
                        "http_request",
                        method = ?request.method(),
                        matched_path,
                        some_other_field = tracing::field::Empty,
                    )
                })
                .on_request(|request: &Request<_>, _span: &Span| {
                    let message = format!(
                        "URI: {}\nHEADERS: {:?}\nMETHOD: {}",
                        request.uri(),
                        request.headers().values(),
                        request.method(),
                    );
                    info!(message);
                })
                .on_response(|_response: &Response, _latency: Duration, _span: &Span| {
                    // ...
                })
                .on_body_chunk(|_chunk: &Bytes, _latency: Duration, _span: &Span| {
                    // ...
                })
                .on_eos(
                    |_trailers: Option<&HeaderMap>, _stream_duration: Duration, _span: &Span| {
                        // ...
                    },
                )
                .on_failure(
                    |error: ServerErrorsFailureClass, _latency: Duration, _span: &Span| {
                        let message = format!("ServerErrorsFailureClass: {error}\n");
                        info!(message);
                    },
                ),
        );

    #[allow(clippy::unwrap_used)]
    let listener = tokio::net::TcpListener::bind(format!("0.0.0.0:{port}"))
        .await
        .unwrap();
    println!("Listening on 0.0.0.0:{port}");
    #[allow(clippy::unwrap_used)]
    axum::serve(listener, app).await.unwrap();
}
